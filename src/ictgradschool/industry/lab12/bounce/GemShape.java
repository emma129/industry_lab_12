package ictgradschool.industry.lab12.bounce;


import java.awt.*;
import javax.swing.*;
/**
 * Created by lelezhao on 4/20/17.
 */
public class GemShape extends Shape {

    public GemShape(){super();}

    public GemShape (int x, int y, int DeltaX, int DeltaY){
        super(x, y, DeltaX, DeltaY);
    }


    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }


    @Override
    public void paint(Painter painter){

        if (fWidth > 40){
            int[] xPoint = {fX+20,fX + (fWidth-20),fX + fWidth,fX + (fWidth-20),fX+20,fX};
            int[] yPoint = {fY,fY,fY + fHeight/2,fY + fHeight,fY + fHeight,fY + fHeight/2};
            painter.drawPolygon(new Polygon(xPoint,yPoint,6));
        }else if (fWidth <= 40){
            painter.drawLine(fX + fWidth /2, fY + 0*fWidth, fX+fWidth,fY+fHeight/2);
            painter.drawLine(fX+fWidth,fY+fHeight/2,fX+fWidth/2, fY +fHeight);
            painter.drawLine(fX+fWidth/2, fY +fHeight,fX + 0*fWidth,fY+fHeight/2);
            painter.drawLine(fX + 0*fWidth,fY+fHeight/2,fX + fWidth /2, fY + 0 *fHeight);
        }

    }

}
