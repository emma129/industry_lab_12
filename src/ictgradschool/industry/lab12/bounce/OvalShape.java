package ictgradschool.industry.lab12.bounce;

/**
 * Created by lelezhao on 4/20/17.
 */
public class OvalShape extends Shape {
    public OvalShape(){super();}

    public OvalShape (int x, int y, int DeltaX, int DeltaY){
        super(x, y, DeltaX, DeltaY);
    }


    public OvalShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }


    @Override
    public void paint(Painter painter) {
        painter.drawOval(fX,fY,fWidth,fHeight);
    }

}
