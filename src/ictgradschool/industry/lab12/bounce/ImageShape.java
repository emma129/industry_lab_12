package ictgradschool.industry.lab12.bounce;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


/**
 * Created by lelezhao on 4/20/17.
 */
public class ImageShape extends Shape {

    private BufferedImage image;
    Image small = null;

    public void importImage(){


        try {
            image = ImageIO.read(new File("src/image/diamond.png"));
            small = image.getScaledInstance(60,60,image.SCALE_DEFAULT);

        }catch (IOException e){
            e.printStackTrace();
        }



    }
    public ImageShape(){
        super();
    }

    public ImageShape (int x, int y, int DeltaX, int DeltaY){
        super(x, y, DeltaX, DeltaY);
        importImage();
    }


    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
        importImage();
    }




    public void paint(Painter painter){

            painter.drawImage(small,fX , fY,fWidth,fHeight, null);

    };
}
