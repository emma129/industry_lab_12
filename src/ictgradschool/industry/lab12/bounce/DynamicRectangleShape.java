package ictgradschool.industry.lab12.bounce;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by lelezhao on 4/20/17.
 */
public class DynamicRectangleShape extends Shape {

    public DynamicRectangleShape() {
        super();
    }

    public DynamicRectangleShape(int x, int y, int DeltaX, int DeltaY) {
        super(x, y, DeltaX, DeltaY);
    }


    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }


    public void paint(Painter painter) {

        if (changeRectangle == 1) {
            painter.setColor(Color.yellow);
            painter.fillRect(fX,fY,fWidth,fHeight);

        }else if (changeRectangle == 2){
            painter.setColor(Color.blue);
            painter.drawRect(fX,fY,fWidth,fHeight);
        }


    }




}




